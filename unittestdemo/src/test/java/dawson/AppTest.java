package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testEcho()
    {
        assertEquals("This test checks if echo returns 5", 5, App.echo(5));
    }

    @Test
    public void testOneMore()
    {
        assertEquals("This test checks if oneMore returns 6", 6, App.oneMore(5));
    }
}
